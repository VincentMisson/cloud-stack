# Cloud Stack Docker

## Organization : 
Tree  
├── README.md  
├── apps  
│   ├── README.md  
│   ├── bitwarden  
│   │   └── docker-compose.yml  
│   ├── freshrss  
│   │   └── docker-compose.yml  
│   ├── ghost  
│   │   └── docker-compose.yml  
│   ├── mail  
│   │   └── docker-compose.yml  
│   ├── nextcloud  
│   │   └── docker-compose.yml  
│   ├── prometheus  
│   │   └── docker-compose.yml  
│   └── statping  
│       └── docker-compose.yml
└── core  
    └── docker-compose.yml  

## Components: 
core : 
- traefik
- portainer
- watchtower

apps :
- bitwarden
- freshrss
- ghost
- mail
    - mailserver
    - rainloop
- nextcloud
    - nextcloud
    - mariadb
    - redis
- prometheus
    - prometheus
    - grafana
    - cadvisor
    - node-exporter
- statping