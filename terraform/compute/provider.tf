terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.117.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "rg-terraform-fra-001"
    storage_account_name = "saterraform002"
    container_name       = "tstate"
    key                  = "compute.tfstate"
  }
}

provider "azurerm" {
  # Configuration options
  features {}
}