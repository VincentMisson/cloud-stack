variable "location" {
  type    = string
  default = "France Central"
}

variable "dc" {
  default = {
    "North Europe"   = "neu"
    "West Europe"    = "weu"
    "France Central" = "fra"
  }
}

variable "id" {
  type    = string
  default = "001"
}

variable "tags" {
  type = map(any)

  default = {
    Environment = "Terraform"
    Resource    = "Infrastructure"
    Application = "Stack"
  }
}

data "azurerm_client_config" "current" {}